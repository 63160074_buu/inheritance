/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.animal;

/**
 *
 * @author f
 */
public class Duck extends Animal{
    private int numberOfWing = 2;
    public Duck (String name, String color){
        super(name, color, 2);
        System.out.println("Duck created");
    }
    @Override
    public void walk() {
        System.out.println("Duck: "+name+" walk with "+numberOfLeg+" leg.");
    }
    @Override
    public void speak() {
        System.out.println("Duck: "+name+" speak > Gab Gab!!");
    }
    public void fly(){
        System.out.println("Duck: "+ name +" fly with "+numberOfWing+" wing.");
    }
    
}
