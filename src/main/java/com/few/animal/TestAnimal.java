/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.animal;

/**
 *
 * @author f
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "White", 0);
        animal.speak();
        animal.walk();
        
        Dog dang = new Dog("Dang", "Black&White");
        dang.speak();
        dang.walk();
        
        Cat zero = new Cat("Zero", "Orange");
        zero.speak();
        zero.walk();
        
        Duck zom = new Duck("Zom", "Orange");
        zom.speak();
        zom.walk();
        zom.fly();

        Dog to = new Dog("To", "Red");
        to.speak();
        to.walk();
        
        Dog mome = new Dog("Mome", "Black&White");
        mome.speak();
        mome.walk();
        
        Dog bat = new Dog("Bat", "Black&White");
        bat.speak();
        bat.walk();
        
        Duck gabgab = new Duck("GabGab", "black");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();

        System.out.println("Zom is Animal: "+(zom instanceof Animal));
        System.out.println("Zom is Duck: "+(zom instanceof Duck));
        System.out.println("Zom is cat: "+(zom instanceof Object));
        System.out.println("Animal is Dog: "+(animal instanceof Dog));
        System.out.println("Animal is Animal: "+(animal instanceof Animal));
        System.out.println("To is Animal: "+(to instanceof Animal));
        System.out.println("To is Cat: "+(to instanceof Object));
        System.out.println("To is Dog: "+(to instanceof Dog));

        Animal[] animals = {dang, zero, zom, to, mome, bat, gabgab};
        for (int i=0;i<animals.length;i++){
            animals[i].walk();
            animals[i].speak();
            if (animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
        }
    }
}
